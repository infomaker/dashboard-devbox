# Upgrading 4.x.x => 5.0.0
Due to changes internally in dashboard a slight configuration needs to be done if upgrading from previous versions of devbox.

### docker-compose.yaml
* service: dasboard
  * `BASE_URL` => `BASE_URL_LIST`
* minio
  * `MINIO_ACCESS_KEY=dblocals3` => `MINIO_ACCESS_KEY=local_aws`
  * `MINIO_SECRET_KEY=dblocals3` => `MINIO_SECRET_KEY=local_aws` 

# Run
To run this you will need a Dockerhub account: https://hub.docker.com/

run:
```
docker login
```


```
docker-compose up
```

Then in your browser go to:
```
http://localhost:8080
```

# Advanced run
In some cases you might want to work towards NavigaID enabled services. In this case you need to do the following.

Update your `hosts` file with the following alias:
```
127.0.0.1       {{ORG_NAME}}.local.dashboard.infomaker.io
```

where the {{ORG_NAME}} is the org you wanna use to login into, example for Naviga org:
```
127.0.0.1       naviga.local.dashboard.infomaker.io
```


### SSL
In order to work towards Naviga ID enabled services we need to run HTTPS locally.

First we need to install a tool to generate and install root and service certificates.
Go to https://github.com/FiloSottile/mkcert and follow the installation instructions for your OS.

Once installed, execute the following steps to generate your first certificate.
```javascript
mkdir ~/certificates 
cd ~/certificates 
mkcert "*.local.dashboard.infomaker.io"
mkcert -install
```

When this is done you should have two different files in your output folder `_wildcard.dashboard.local.infomaker.io-key.pem` as well as `_wildcard.local.dashboard.infomaker.io.pem`

The content of these files needs to be added to the corresponding files in this repo. Under `proxy-ssl` folder we have a key and a cert file which currently are empty.

Take the content of the `_wildcard.dashboard.local.infomaker.io-key.pem` and put it in the `./proxy-ssl/key.pem` file

Do the same with the `_wildcard.local.dashboard.infomaker.io.pem` but put it into `./proxy-ssl/cert.pem`



Go to `docker-compose-ssl.yaml` file and update following:
-   Uncomment services/dashboard/environment `DEFAULT_ORGANISATION_TO_USE` and add the organisation you want to use
-   Go to https://demo.stage.imid.infomaker.io/open and login towards the intended organisation to use
-   Replace the bracketed values below in SERVICE_TOKEN_PAYLOAD with corresponding data from the site above.
```javascript
{
    "org": "{{ORG_NAME}}",
    "sub": "{{ORG_NAME}}_{{SUB_ID}}",
    "groups": [],
    "permissions": {
        "org": [],
        "units": {
        "{{UNIT}}": [
            "dashboard:user/me"
            "dashboard:group/admin",
            "dashboard:group/powerUser",
            "dashboard:group/user",
            "dashboard:group/readOnly",
            "dashboard:dashboard/load",
            "dashboard:plugin/install",
            "dashboard:plugin/update",
            "dashboard:plugin/delete",
            "dashboard:profile/create",
            "dashboard:profile/delete",
            "dashboard:profile/update",
            "dashboard:storage/delete",
            "dashboard:storage/get",
            "dashboard:storage/save",
            "dashboard:user/me",
            "dashboard:config-key/create",
            "dashboard:config-key/delete",
            "dashboard:config-key/update",
            "dashboard:workspace/create",
            "dashboard:workspace/delete",
            "dashboard:workspace/update",
            "dashboard:backup/import",
            "dashboard:backup/export",
            "dashboard:backup/validate"
        ]
        }
    },
    "userinfo": {
        "given_name": "Dashboard",
        "family_name": "User",
        "email": "dashboard-user@infomaker.io"
    }
}
```
run `docker-compose -f docker-compose-ssl.yaml up`

You should now be able to visit `https://{{ORG_NAME}}.local.dashboard.infomaker.io`